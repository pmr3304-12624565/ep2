from django.contrib import admin
from aleuni import views
from django.urls import include, path

urlpatterns = [
    # path('', views.PostList.as_view(), name="home"),
    # path('<slug:slug>/', views.PostList.as_view(), name="post_detail"),
    # path('login/', views.SignUpView.as_view(), name="login"),
    # path('pol/', views.PolView.as_view(), name="politica"),
    # path('con/', views.ConView.as_view(), name="contato"),
    # path('rec/', views.RecView.as_view(), name="reclamacoes"),

    
    # path('', views.post_list, name='post_list'),
    # path('post/<int:post_id>/', views.post_detail, name='post_detail'),
    # path('post/create/', views.post_create, name='post_create'),
    # path('post/<int:post_id>/update/', views.post_update, name='post_update'),
    # path('post/<int:post_id>/delete/', views.post_delete, name='post_delete'),

    path('', views.post_list, name='post_list'),
    path('post/<int:post_id>/', views.post_detail, name='post_detail'),
    path('post/create/', views.post_create, name='post_create'),
    path('post/<int:post_id>/update/', views.post_update, name='post_update'),
    path('post/<int:post_id>/delete/', views.post_delete, name='post_delete'),
    
]