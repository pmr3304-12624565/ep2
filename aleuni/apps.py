from django.apps import AppConfig


class AleuniConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aleuni'
